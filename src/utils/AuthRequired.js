import * as jwt from 'jsonwebtoken'
import store from "../store";

export default async (to, from, next) => {
  const token = $cookies.get('token');
  if (token) {
    const decodedToken = jwt.decode(token);

    if (decodedToken && decodedToken.exp * 1000 < Date.now()) {
      // Token has expired
      store.dispatch('signOut', 'timeout');
    } else {
      // Token is still valid
      store.commit('setUser', decodedToken);
      next();
    }
  } else {
    // No token found
    store.dispatch('signOut', 'timeout');
  }
}
