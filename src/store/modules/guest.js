import axios from "axios";
import store from "..";

export default {
  state: {
    guest: null
  },
  getters: {
    guest: (state) => state.guest
  },
  mutations: {
    setGuest(state, payload) {
      state.guest = payload;
    }
  },
  actions: {
    async getGuest({ commit }, id) {
      try {
        const response = await axios.get(
          `${process.env.VUE_APP_API_ENDPOINT}/guest-detail/${id}`,
          {
            headers: { Authorization: `Bearer ${$cookies.get("token")}` },
          }
        );        
        if (response.data.success) {
          commit("setGuest", response.data.data);
        }
      } catch (error) {
        console.error(error);
        if (error.response.status === 401) {
          store.dispatch('signOut', 'timeout');
        }
      }
    },
    async deleteGuest(_, id) {
      try {
        await axios.delete(
          `${process.env.VUE_APP_API_ENDPOINT}/delete-guest/${id}`,
          {
            headers: { Authorization: `Bearer ${$cookies.get("token")}` },
          }
        );
      } catch (error) {
        console.error(error);
        if (error.response.status === 401) {
          store.dispatch('signOut', 'timeout');
        }
      }
    },
    async editGuest({ commit }, payload) {
      try {
        const response = await axios.put(
          `${process.env.VUE_APP_API_ENDPOINT}/update-guest/${payload.id}`,
          payload,
          {
            headers: { Authorization: `Bearer ${$cookies.get("token")}` },
          }
        );
        commit("setGuest", response.data);
      } catch (error) {
        if (error.response.status === 401) {
          store.dispatch('signOut', 'timeout');
        }
        throw error;
      }
    }
  },
};
